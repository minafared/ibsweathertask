package com.ibs.Weather.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mina fared on 10/17/2017.
 */
public class WeatherDetails implements Serializable {
    public int id;
    public String name;
    public Coord coord;
    public Main main;
    public int dt;
    public Wind wind;
    public Sys sys;
    public Clouds clouds;
    public List<Weather> weather;
}
