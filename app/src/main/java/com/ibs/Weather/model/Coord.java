package com.ibs.Weather.model;

import java.io.Serializable;

/**
 * Created by mina fared on 10/17/2017.
 */
public class Coord implements Serializable {
    public double lat;
    public double lon;
}
