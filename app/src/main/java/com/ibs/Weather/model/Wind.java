package com.ibs.Weather.model;

import java.io.Serializable;

/**
 * Created by mina fared on 10/17/2017.
 */
public class Wind implements Serializable {
    public double speed;
    public double deg;
}
