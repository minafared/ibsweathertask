package com.ibs.Weather.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mina fared on 10/17/2017.
 */

@SuppressWarnings("all")
public class WeatherResponse implements Serializable {

    public String message;
    public String cod;
    public int count;
    public List<WeatherDetails> list;


}
