package com.ibs.Weather.ui.map;


import android.content.Context;

import com.ibs.Weather.app.Constants;
import com.ibs.Weather.model.WeatherDetails;
import com.ibs.Weather.model.WeatherResponse;
import com.ibs.Weather.network.Api;
import com.ibs.Weather.utils.Utils;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by mina fared on 10/17/2017.
 */

public class CitiesPresenterImpl implements CitiesPresenter {

    private CitiesView view;
    private boolean isUpdating;
    private Context context;

    @Override
    public void setView(Context context, CitiesView view) {
        this.view = view;
        this.context = context;
    }

    /*
    * get place  from google Weather  api
     */
    @Override
    public void getPlaces(String latitude, String longitude) {

        // to prevent more than request at the same time
        if (isUpdating) {
            return;
        }
        isUpdating = true;
        view.showLoading();

        if (!Utils.isOnline(context)) {
            view.hideLoading();
            return;
        }


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);
        Observable<WeatherResponse> friendResponseObservable = api.getWeather(latitude, longitude, Constants.cnt, Constants.units, Constants.KEY)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        friendResponseObservable.subscribe(new Observer<WeatherResponse>() {


            @Override
            public void onError(Throwable e) {
                view.showErrorMessage();
                view.hideLoading();
                // enable update again
                isUpdating = false;
            }

            @Override
            public void onComplete() {
                view.hideLoading();
                // enable update again
                isUpdating = false;
            }

            @Override
            public void onSubscribe(Disposable d) {
            }

            @Override
            public void onNext(WeatherResponse response) {

                if (response.list.size() > 0) {

                    List<WeatherDetails> items = response.list;
                    if (items.size() > 0) {
                        view.showPlaces(response.list);
                    } else
                        view.showEmptyResult();

                } else
                    view.showEmptyResult();
            }
        });


    }


}
