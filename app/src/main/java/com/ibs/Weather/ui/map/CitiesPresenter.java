package com.ibs.Weather.ui.map;

import android.content.Context;

/**
 * Created by mina fared on 10/17/2017.
 */

public interface CitiesPresenter {

    void setView(Context context, CitiesView view);

    void getPlaces(String latitude, String longitude);
 }
