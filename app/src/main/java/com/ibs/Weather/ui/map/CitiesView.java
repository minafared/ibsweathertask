package com.ibs.Weather.ui.map;

import com.ibs.Weather.model.WeatherDetails;

import java.util.List;

/**
 * Created by mina fared on 10/17/2017.
 */

public interface CitiesView {

    void showLoading();

    void hideLoading();

    void showPlaces(List<WeatherDetails> venues);

    void showErrorMessage();

    void showEmptyResult();

}
