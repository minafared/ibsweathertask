package com.ibs.Weather.network;

import com.ibs.Weather.model.WeatherResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by mina fared on 10/17/2017.
 */
public interface Api {

    @GET("find?")
    Observable<WeatherResponse> getWeather(@Query("lat") String lat,
                                           @Query("lon") String lon,
                                           @Query("cnt") int cnt,
                                           @Query("units") String units,
                                           @Query("APPID") String APPID);


}
