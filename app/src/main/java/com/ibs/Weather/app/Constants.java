package com.ibs.Weather.app;

/**
 * Created by mina fared on 10/14/2017.
 */

public class Constants {

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";

    public static final String KEY = "d1662ddaf9ecb9b664ad6905a948fc24";

    public static final int cnt = 50; //50 record per page.


    public static final int LOCATION_PERMISSION_CODE = 1000;

    public static final String PREFS = "prefs";
    public static final String units = "metric";

    public static final String  photoBaseUrl = "http://openweathermap.org/img/w/";


 }
